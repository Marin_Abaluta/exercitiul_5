package ro.orangeprojects6;

public interface MyInterface {

    public abstract int CalculateSize();

    public abstract String getStyle();
}
