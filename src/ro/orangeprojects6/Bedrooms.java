package ro.orangeprojects6;

public class Bedrooms {

    public static void main(String[] args) {


        Hotel_Bedrooms b1 = new Hotel_Bedrooms(300, 50, 52, "Headboard and Side Rails");
        Hotel_Bedrooms b2 = new Hotel_Bedrooms(250, 100, 40, "Swedish");

        System.out.println(b1.CalculateSize());
        System.out.println(b2.CalculateSize());
        System.out.println(b1.getStyle());
        System.out.println(b2.getStyle());

    }
}
